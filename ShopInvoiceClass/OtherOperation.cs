﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopInvoiceClass
{
    public class OtherOperation
    {
        private int id;
        // 1 - in 2 - out
        private int type;
        private DateTime date;
        private int productid;
        private int quantity;

        public int Id { get => id; set => id = value; }
        public int Type { get => type; set => type = value; }
        public DateTime Date { get => date; set => date = value; }
        public int Productid { get => productid; set => productid = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public OtherOperation(int type,int productid,int quantity)
        {
            this.type = type;
            this.productid = productid;
            this.quantity = quantity;
        }
        public OtherOperation(int id,int type,DateTime date,int productid,int quantity)
        {
            this.id = id;
            this.type = type;
            this.date = date;
            this.productid = productid;
            this.quantity = quantity;
        }
    }
}
