﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ShopInvoiceClass
{
    public static class DBProducts
    {
        private static List<Product> productList = new List<Product>();
        public static int Insert(Product p)
        {
            int affectedRow = 0;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO [Products] ([Id],[Name],[Price],[PTU],[PKWiU],[Quantity]) VALUES (@id,@name,@price,@ptu,@pkwiu,@quantity)", con);
                cmd.Parameters.AddWithValue("@id", p.Id);
                cmd.Parameters.AddWithValue("@name", p.Name);
                cmd.Parameters.AddWithValue("@price", p.Price);
                cmd.Parameters.AddWithValue("@ptu", p.PTU);
                cmd.Parameters.AddWithValue("@pkwiu", p.PKWiU);
                cmd.Parameters.AddWithValue("@quantity", p.Quantity);
                affectedRow = cmd.ExecuteNonQuery();
            }
            return affectedRow;
        }
        public static int Count
        {
            get
            {
                int count = 0;
                using (DBConnection db = new())
                {
                    db.GetConnection.Open();
                    count = db.Count("Products");
                }
                return count;
            }
        }
        public static int Edit(Product p)
        {
            int affectedRows;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("UPDATE [Products] SET [Name] = @name, [Price] = @price, [PTU] = @ptu,[PKWiU] = @pkwiu WHERE [Id] = @id", con);
                cmd.Parameters.AddWithValue("@name", p.Name);
                cmd.Parameters.AddWithValue("@price", p.Price);
                cmd.Parameters.AddWithValue("@ptu", p.PTU);
                cmd.Parameters.AddWithValue("@pkwiu", p.PKWiU);
                cmd.Parameters.AddWithValue("@id", p.Id);
                affectedRows = cmd.ExecuteNonQuery();
            }
            return affectedRows;
        }
        public static void Delete(int id)
        {
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                db.Delete("Products", id);
            }
        }
        public static List<Product> GetProducts(int pageIndex)
        {
            productList.Clear();
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.Select("Products", pageIndex))
                {
                    while (reader.Read())
                    {
                        productList.Add(new Product((int)reader["Id"], reader["Name"].ToString(), (decimal)reader["Price"], (short)reader["PTU"], (int)reader["PKWiU"], ((int)reader["Quantity"]) + Quantity((int)reader["Id"])));
                    }
                }
            }
            return productList;
        }
        public static List<Product> GetProducts()
        {
            return productList;
        }
        public static Product GetProduct(int id)
        {
            Product prod = null;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.GetByID("Products", id))
                {
                    reader.Read();
                    if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                        prod = new Product((int)reader["Id"], reader["Name"].ToString(), (decimal)reader["Price"], (short)reader["PTU"], (int)reader["PKWiU"], ((int)reader["Quantity"]) + Quantity((int)reader["Id"]));
                }
            }
            return prod;
        }
        public static int LastId()
        {
            int tmp = 0;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                tmp = db.LastID("Products");
            }
            return tmp;
        }
        public static int Quantity(int id)
        {
            int quantity = 0;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT SUM([Quantity]) AS 'Quantity' FROM [InvoiceProducts] WHERE [ProductID] = @id", con);
                cmd.Parameters.AddWithValue("@id", id);
                using (SqlDataReader reader1 = cmd.ExecuteReader())
                {
                    reader1.Read();
                    if (!reader1.IsDBNull(reader1.GetOrdinal("Quantity")))
                        quantity -= Convert.ToInt32(reader1["Quantity"]);
                }
                cmd = new SqlCommand("SELECT [Type], SUM([Quantity]) AS 'Quantity' FROM [OtherOperations] WHERE [ProductID] = @id GROUP BY [Type]", con);
                cmd.Parameters.AddWithValue("@id", id);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("Type")))
                        {
                            if (Convert.ToInt32(reader["Type"]) == 1)
                            {
                                quantity += Convert.ToInt32(reader["Quantity"]);
                            }
                            else if (Convert.ToInt32(reader["Type"]) == 2)
                            {
                                quantity -= Convert.ToInt32(reader["Quantity"]);
                            }
                        }
                    }
                }
            }
            return quantity;
        }
    }
}
