﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ShopInvoiceClass
{
    public static class DBContractor
    {
        private static List<Contractor> contractorList = new();

        public static List<Contractor> GetContractors(int pageIndex)
        {
            contractorList.Clear();
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.Select("Contractors", pageIndex))
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                            contractorList.Add(new Contractor(Convert.ToInt32(reader["Id"]), reader["Name"].ToString(), reader["Address"].ToString(), reader["NIP"].ToString(), reader["REGON"].ToString()));
                    }
                }
            }
            return contractorList;
        }
        public static List<Contractor> GetContractors()
        {
            contractorList.Clear();
            using (DBConnection db = new DBConnection())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.Select("Contractors"))
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                            contractorList.Add(new Contractor(Convert.ToInt32(reader["Id"]), reader["Name"].ToString(), reader["Address"].ToString(), reader["NIP"].ToString(), reader["REGON"].ToString()));
                    }
                }
            }
            return contractorList;
        }
        public static List<Contractor> GetList()
        {
            return contractorList;
        }
        public static void Insert(Contractor c)
        {
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("Insert INTO [Contractors] ([Id], [Name],[Address],[REGON],[NIP]) VALUES (@id, @name, @address, @regon, @nip)", con);
                cmd.Parameters.AddWithValue("@id", c.ID);
                cmd.Parameters.AddWithValue("@name", c.Name);
                cmd.Parameters.AddWithValue("@address", c.Address);
                cmd.Parameters.AddWithValue("@regon", c.REGON);
                cmd.Parameters.AddWithValue("@nip", c.NIP);
                cmd.ExecuteNonQuery();
            }
        }
        public static Contractor GetByID(int id)
        {
            Contractor result = null;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.GetByID("Contractors", id))
                {
                    reader.Read();
                    if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                        result = new Contractor(Convert.ToInt32(reader["Id"]), reader["Name"].ToString(), reader["Address"].ToString(), reader["NIP"].ToString(), reader["REGON"].ToString());
                }
            }
            return result;
        }
        public static void Delete(int id)
        {
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                db.Delete("Contractors", id);
            }
        }
        public static int EditByID(Contractor c)
        {
            int affectedRows;
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                SqlCommand cmd = new("UPDATE [Contractors] SET [Name] = @name, [Address] = @address, [NIP] = @nip,[REGON] = @regon WHERE [Id] = @id", con);
                cmd.Parameters.AddWithValue("@name", c.Name);
                cmd.Parameters.AddWithValue("@address", c.Address);
                cmd.Parameters.AddWithValue("@nip", c.NIP);
                cmd.Parameters.AddWithValue("@regon", c.REGON);
                cmd.Parameters.AddWithValue("@id", c.ID);
                affectedRows = cmd.ExecuteNonQuery();
            }
            return affectedRows;
        }
        public static int Count
        {
            get
            {
                int tmp;
                using (DBConnection db = new())
                {
                    db.GetConnection.Open();
                    tmp = db.Count("Contractors");
                }
                return tmp;
            }
        }
        public static int LastId()
        {
            int tmp = 0;
            using (DBConnection db = new DBConnection())
            {
                db.GetConnection.Open();
                tmp = db.LastID("Contractors");
            }
            return tmp;
        }
    }
}
