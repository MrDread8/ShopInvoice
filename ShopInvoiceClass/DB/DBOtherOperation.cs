﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopInvoiceClass.DB
{
    public static class DBOtherOperation
    {
        public static void Insert(List<OtherOperation> operations)
        {
            using (SqlConnection con = new DBConnection().GetConnection)
            {
                con.Open();
                foreach (OtherOperation op in operations)
                {
                    SqlCommand cmd = new("INSERT INTO [OtherOperations] VALUES(@Id,@type,@date,@productID,@quantity)", con);
                    cmd.Parameters.AddWithValue("@id", LastIndex() + 1);
                    cmd.Parameters.AddWithValue("@type", op.Type);
                    cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm")));
                    cmd.Parameters.AddWithValue("@productID", op.Productid);
                    cmd.Parameters.AddWithValue("@quantity", op.Quantity);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static int LastIndex()
        {
            int tmp = 0;
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                tmp = db.LastID("OtherOperations");
            }
            return tmp;
        }
        public static int Count
        {
            get
            {
                int tmp = 0;
                using (DBConnection db = new())
                {
                    db.GetConnection.Open();
                    tmp = db.LastID("OtherOperations");
                }
                return tmp;
            }
        }
        public static List<OtherOperation> Get(int pageIndex)
        {
            List<OtherOperation> operations = new();
            using (DBConnection db = new())
            {
                db.GetConnection.Open();
                using (SqlDataReader reader = db.Select("OtherOperations", pageIndex))
                {
                    while (reader.Read())
                    {
                        operations.Add(new OtherOperation(Convert.ToInt32(reader["Id"]), Convert.ToInt32(reader["Type"]), Convert.ToDateTime(reader["Date"]), Convert.ToInt32(reader["ProductID"]), Convert.ToInt32(reader["Quantity"])));
                    }
                }
            }
            return operations;
        }
    }
}
