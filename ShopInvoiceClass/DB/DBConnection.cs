﻿using System;
using System.Data.SqlClient;

namespace ShopInvoiceClass
{
    public class DBConnection : IDisposable
    {
        private SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\drpla\Documents\Projects\Invoice\ShopInvoiceClass\DataBase.mdf;Integrated Security=True");
        public SqlConnection GetConnection
        {
            get => connection;
        }
        public void Dispose()
        {

        }
        public SqlDataReader Select(string tableName)
        {
            SqlCommand cmd = new(string.Format("SELECT * FROM {0}", tableName), connection);
            cmd.Parameters.AddWithValue("@tableName", tableName);
            return cmd.ExecuteReader();
        }
        public SqlDataReader Select(string tableName, int pageNumber)
        {
            SqlCommand cmd = new(string.Format("SELECT * FROM {0} ORDER BY [Id] OFFSET {1} ROWS FETCH NEXT 10 ROWS ONLY", tableName, (pageNumber * 10 - 10)), connection);
            return cmd.ExecuteReader();
        }
        public SqlDataReader GetByID(string tableName, int id)
        {
            SqlCommand cmd = new(string.Format("SELECT * FROM {0} WHERE [Id] = {1}", tableName, id), connection);
            return cmd.ExecuteReader();
        }
        public int Count(string tableName)
        {
            int tmp = 0;
            SqlCommand cmd = new(string.Format("SELECT COUNT([Id]) AS 'Count' FROM {0}", tableName), connection);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                if (!reader.IsDBNull(reader.GetOrdinal("Count")))
                    tmp = Convert.ToInt32(reader["Count"]);
            }
            return tmp;
        }
        public int LastID(string tableName)
        {
            int tmp = 0;
            SqlCommand cmd = new(string.Format("SELECT MAX([Id]) AS 'LastId' FROM {0}", tableName), connection);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                if (!reader.IsDBNull(reader.GetOrdinal("LastId")))
                    tmp = Convert.ToInt32(reader["LastId"]);
            }
            return tmp;
        }
        public void Delete(string tableName, int id)
        {
            SqlCommand cmd = new(string.Format("DELETE FROM {0} WHERE [Id] = {1}",tableName,id), connection);
            cmd.ExecuteNonQuery();
        }
    }
}
