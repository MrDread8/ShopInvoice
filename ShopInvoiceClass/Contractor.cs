﻿using System.Runtime.CompilerServices;

namespace ShopInvoiceClass
{
    public class Contractor
    {
        private int id;
        private string name;
        private string address;
        private string nip;
        private string regon;
        public Contractor(int ID, string Name, string Address, string Nip, string Regon)
        {
            this.id = ID;
            this.Name = Name;
            this.address = Address;
            this.nip = Nip;
            this.regon = Regon;
        }
        public int ID
        {
            get => this.id;
            set => this.id = value;
        }
        public string Name { get => name; set => name = value; }

        public string Address
        {
            get => this.address;
            set => this.address = value;
        }
        public string NIP
        {
            get => this.nip;
            set => this.nip = value;
        }
        public string REGON
        {
            get => this.regon;
            set => this.regon = value;
        }
        public static bool operator ==(Contractor a, Contractor b)
        {
            if (a.ID == b.ID)
                if (a.Name == b.Name)
                    if (a.Address == b.Address)
                        if (a.NIP == b.NIP)
                            if (a.REGON == b.REGON)
                                return true;

            return false;
        }
        public static bool operator !=(Contractor a, Contractor b)
        {
            if (a.ID != b.ID)
                if (a.Name != b.Name)
                    if (a.Address != b.Address)
                        if (a.NIP != b.NIP)
                            if (a.REGON != b.REGON)
                                return true;

            return false;
        }

        public override bool Equals(object obj)
        {
            Contractor a = obj as Contractor;
            if (a.ID != id)
                if (a.Name != name)
                    if (a.Address != address)
                        if (a.NIP != nip)
                            if (a.REGON != regon)
                                return true;

            return false;
        }
    }
}
