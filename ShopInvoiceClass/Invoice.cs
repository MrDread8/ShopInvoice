﻿using System;
using System.Collections.Generic;

namespace ShopInvoiceClass
{
    public class Invoice
    {
        private int id;
        private string name;
        private int contractorID;
        private decimal charge = 0.00m;
        private List<Product> invoiceProducts = new();

        public Invoice(int id, string name, int contractorID, decimal sum)
        {
            this.id = id;
            this.name = name;
            this.contractorID = contractorID;
            this.charge = sum;
        }
        public Invoice(int id, int number, int contractorID, decimal sum)
        {
            this.id = id;
            this.contractorID = contractorID;
            this.charge = sum;
            this.name = DateTime.Now.ToString("yyy/MM/dd/") + String.Format("{0:0000}", number);
        }
        public Invoice()
        {

        }
        public int Id
        {
            get => this.id; set => this.id = value;
        }
        public string Name
        {
            get => this.name; set => this.name = this.name = DateTime.Now.ToString("yyy/MM/dd/") + String.Format("{0:0000}", value);
        }
        public int ContractorID
        {
            get => this.contractorID; set => this.contractorID = value;
        }
        public decimal Charge
        {
            get => this.charge; set => this.charge = value;
        }
        public List<Product> InvoiceProducts
        {
            get => this.invoiceProducts;
        }
    }
}
