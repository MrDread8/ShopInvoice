# Aplikacja Do Fakturowania
## Wykorzystane technologie
- ASP.NET
- .NET Framework
- MySQL
## Funkcjonalności
- Lista kontrahentów
	- Edytowanie
	- Usuwanie
	- Dodawanie
	- Wyświetlanie faktur dla danego kontrahenta
- Faktury
	- Dodawanie
	- Lista produktów
	- Obliczanie ceny brutto
	- Podgląd faktury na stronie www
- Produkty
	- Dodawanie
	- Usuwanie
	- Edycja
	- Wybór kodu PKWiU
	- Ustawianie stawki PTU
- Zarządzanie Stanem Magazynowym
	- Dostawa
	- Operacje poza fakturowe
	- Wyświetlanie aktualnego stanu magazynowego na stronie www
![app new contractor](./AppNewContractor.gif)
![app new product anim](./AppNewProduct.gif)
![web invoice](./WebInvoice.png)
![web storage](./WebStorage.png)
