using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopInvoiceClass;

namespace ShopInvoice.UnitTest
{
    [TestClass]
    public class ContractorTest
    {
        [TestMethod]
        public void InsertContractor_InsertsNewContractor_ReutrnsAffectedRowsCount()
        {
            //Arrange
            Contractor testContractor = new(99, "TestContractor", "Blotna55", "123123", "232323");
            Contractor result;
            //Act
            DBContractor.Insert(testContractor);
            result = DBContractor.GetByID(99);
            //Assert
            Assert.IsTrue(result == testContractor);
        }
        [TestMethod]
        public void EditContractor_EditContractorInDB_ReturnsAffectedRowsCount()
        {
            //Arrange
            Contractor testContractor = new(99, "TestContractor", "Blotna55", "123123", "232323");
            //Act
            var result = DBContractor.EditByID(testContractor);
            //Assert
            Assert.IsTrue(result == 0 || result == 1);
        }
        [TestMethod]
        public void DropContracotor_DropContractor_ReutrnAffectedRows()
        {
            //Arrange

            //Act
            var result = DBContractor.Delete(99);
            //Assert
            Assert.IsTrue(result == 0 || result == 1);
        }
    }
}
