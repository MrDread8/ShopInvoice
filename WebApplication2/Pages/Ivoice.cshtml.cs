using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ShopInvoiceClass;
using System.Threading.Tasks;

namespace WebApplication2.Pages
{
    public class IvoiceModel : PageModel
    {
        [BindProperty]
        public int ID { get; set; }
        public Invoice invo;
        public Contractor ContractorDate;
        public decimal sum8;
        public decimal sum5;
        public decimal sum23;
        public void OnGet(int id)
        {
            ID = id;
            invo = DBInvoice.GetByID(id);
            ContractorDate = DBContractor.GetByID(invo.ContractorID);
            foreach (Product p in invo.InvoiceProducts)
            {
                if (p.PTU == 5)
                {
                    sum5 += DBProducts.GetProduct(p.Id).Price * p.Quantity;
                }
                else if (p.PTU == 8)
                {
                    sum8 += DBProducts.GetProduct(p.Id).Price * p.Quantity;
                }
                else
                {
                    sum23 += DBProducts.GetProduct(p.Id).Price * p.Quantity;
                }
            }
        }
    }
}
