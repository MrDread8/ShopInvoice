﻿
namespace ShopInvoiceWidows.Products
{
    partial class SelectPkwiu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridPKWiU = new System.Windows.Forms.DataGridView();
            this.symbolID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Symbol = new System.Windows.Forms.Label();
            this.formBadge = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.formName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridPKWiU)).BeginInit();
            this.SuspendLayout();
            // 
            // gridPKWiU
            // 
            this.gridPKWiU.AllowDrop = true;
            this.gridPKWiU.AllowUserToAddRows = false;
            this.gridPKWiU.AllowUserToDeleteRows = false;
            this.gridPKWiU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPKWiU.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.symbolID,
            this.gridSymbol,
            this.gridName});
            this.gridPKWiU.Location = new System.Drawing.Point(13, 39);
            this.gridPKWiU.MultiSelect = false;
            this.gridPKWiU.Name = "gridPKWiU";
            this.gridPKWiU.ReadOnly = true;
            this.gridPKWiU.RowTemplate.Height = 25;
            this.gridPKWiU.Size = new System.Drawing.Size(775, 399);
            this.gridPKWiU.TabIndex = 0;
            this.gridPKWiU.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridPKWiU_RowHeaderMouseClick);
            // 
            // symbolID
            // 
            this.symbolID.HeaderText = "ID";
            this.symbolID.Name = "symbolID";
            this.symbolID.ReadOnly = true;
            // 
            // gridSymbol
            // 
            this.gridSymbol.HeaderText = "Symbol";
            this.gridSymbol.Name = "gridSymbol";
            this.gridSymbol.ReadOnly = true;
            // 
            // gridName
            // 
            this.gridName.HeaderText = "Nazwa";
            this.gridName.Name = "gridName";
            this.gridName.ReadOnly = true;
            // 
            // Symbol
            // 
            this.Symbol.AutoSize = true;
            this.Symbol.Location = new System.Drawing.Point(13, 13);
            this.Symbol.Name = "Symbol";
            this.Symbol.Size = new System.Drawing.Size(47, 15);
            this.Symbol.TabIndex = 1;
            this.Symbol.Text = "Symbol";
            // 
            // formBadge
            // 
            this.formBadge.Location = new System.Drawing.Point(66, 10);
            this.formBadge.Name = "formBadge";
            this.formBadge.Size = new System.Drawing.Size(145, 23);
            this.formBadge.TabIndex = 2;
            this.formBadge.TextChanged += new System.EventHandler(this.formBadge_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(217, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nazwa";
            // 
            // formName
            // 
            this.formName.Location = new System.Drawing.Point(265, 10);
            this.formName.Name = "formName";
            this.formName.Size = new System.Drawing.Size(163, 23);
            this.formName.TabIndex = 4;
            this.formName.TextChanged += new System.EventHandler(this.formName_TextChanged);
            // 
            // SelectPkwiu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.formName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.formBadge);
            this.Controls.Add(this.Symbol);
            this.Controls.Add(this.gridPKWiU);
            this.Name = "SelectPkwiu";
            this.Text = "Wyszukiwarka PKWIU";
            ((System.ComponentModel.ISupportInitialize)(this.gridPKWiU)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridPKWiU;
        private System.Windows.Forms.Label Symbol;
        private System.Windows.Forms.TextBox formBadge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox formName;
        private System.Windows.Forms.DataGridViewTextBoxColumn symbolID;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridName;
    }
}