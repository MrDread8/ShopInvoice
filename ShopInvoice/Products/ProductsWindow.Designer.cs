﻿
namespace ShopInvoiceWidows.Products
{
    partial class ProductsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productsGrid = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prodName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PTU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridPKWiU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.AddButoon = new System.Windows.Forms.ToolStripMenuItem();
            this.EditButton = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveButton = new System.Windows.Forms.ToolStripMenuItem();
            this.nextButton = new System.Windows.Forms.Button();
            this.prevButton = new System.Windows.Forms.Button();
            this.pageID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productsGrid)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // productsGrid
            // 
            this.productsGrid.AllowUserToAddRows = false;
            this.productsGrid.AllowUserToDeleteRows = false;
            this.productsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.prodName,
            this.Price,
            this.PTU,
            this.gridPKWiU,
            this.Quantity});
            this.productsGrid.Location = new System.Drawing.Point(12, 27);
            this.productsGrid.MultiSelect = false;
            this.productsGrid.Name = "productsGrid";
            this.productsGrid.ReadOnly = true;
            this.productsGrid.RowTemplate.Height = 25;
            this.productsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.productsGrid.Size = new System.Drawing.Size(947, 498);
            this.productsGrid.TabIndex = 2;
            this.productsGrid.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.productsGrid_RowHeaderMouseClick);
            // 
            // Id
            // 
            this.Id.HeaderText = "ID";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // prodName
            // 
            this.prodName.HeaderText = "Nazwa";
            this.prodName.Name = "prodName";
            this.prodName.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.HeaderText = "Cena";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // PTU
            // 
            this.PTU.HeaderText = "PTU";
            this.PTU.Name = "PTU";
            this.PTU.ReadOnly = true;
            // 
            // gridPKWiU
            // 
            this.gridPKWiU.HeaderText = "PKWiU";
            this.gridPKWiU.Name = "gridPKWiU";
            this.gridPKWiU.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Ilosc";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddButoon,
            this.EditButton,
            this.RemoveButton});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(971, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // AddButoon
            // 
            this.AddButoon.Name = "AddButoon";
            this.AddButoon.Size = new System.Drawing.Size(41, 20);
            this.AddButoon.Tag = "AddButton";
            this.AddButoon.Text = "Add";
            this.AddButoon.Click += new System.EventHandler(this.AddButoon_Click);
            // 
            // EditButton
            // 
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(39, 20);
            this.EditButton.Tag = "EditButoon";
            this.EditButton.Text = "Edit";
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // RemoveButton
            // 
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(62, 20);
            this.RemoveButton.Tag = "RemoveButton";
            this.RemoveButton.Text = "Remove";
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(510, 533);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 24);
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = ">>";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(386, 533);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 24);
            this.prevButton.TabIndex = 6;
            this.prevButton.Text = "<<";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // pageID
            // 
            this.pageID.AutoSize = true;
            this.pageID.Location = new System.Drawing.Point(479, 537);
            this.pageID.Name = "pageID";
            this.pageID.Size = new System.Drawing.Size(13, 15);
            this.pageID.TabIndex = 5;
            this.pageID.Text = "1";
            // 
            // ProductsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 564);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.pageID);
            this.Controls.Add(this.productsGrid);
            this.Controls.Add(this.menuStrip1);
            this.Name = "ProductsWindow";
            this.Text = "Produkty";
            ((System.ComponentModel.ISupportInitialize)(this.productsGrid)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView productsGrid;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem AddButoon;
        private System.Windows.Forms.ToolStripMenuItem EditButton;
        private System.Windows.Forms.ToolStripMenuItem RemoveButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Label pageID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn prodName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn PTU;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridPKWiU;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
    }
}