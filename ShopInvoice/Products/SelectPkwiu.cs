﻿using ShopInvoiceClass;
using System;
using System.Windows.Forms;

namespace ShopInvoiceWidows.Products
{
    public partial class SelectPkwiu : Form
    {
        public SelectPkwiu()
        {
            InitializeComponent();
        }
        private int id = 0;

        public int ID
        {
            get => id;
            set => id = value;
        }
        private void formBadge_TextChanged(object sender, EventArgs e)
        {
            search();
        }

        private void formName_TextChanged(object sender, EventArgs e)
        {
            search();
        }
        private void search()
        {
            gridPKWiU.Rows.Clear();

            if (String.IsNullOrEmpty(formBadge.Text) ^ String.IsNullOrEmpty(formName.Text))
            {
                if (!String.IsNullOrEmpty(formBadge.Text))
                {
                    foreach (PKWiU p in DBPKWiU.GetByBadge(formBadge.Text))
                    {
                        gridPKWiU.Rows.Add(p.ID.ToString(), p.Badge.ToString(), p.Name.ToString());
                    }
                }
                else
                {
                    foreach (PKWiU p in DBPKWiU.GetByName(formName.Text))
                    {
                        gridPKWiU.Rows.Add(p.ID.ToString(), p.Badge.ToString(), p.Name.ToString());
                    }
                }
            }
            else if (!String.IsNullOrEmpty(formBadge.Text) && !String.IsNullOrEmpty(formName.Text))
            {
                foreach (PKWiU p in DBPKWiU.GetByNameAndBadge(formName.Text, formBadge.Text))
                {
                    gridPKWiU.Rows.Add(p.ID.ToString(), p.Badge.ToString(), p.Name.ToString());
                }
            }
        }

        private void gridPKWiU_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            id = Convert.ToInt32(gridPKWiU.SelectedRows[0].Cells[0].Value);
            this.Close();
        }
    }
}
