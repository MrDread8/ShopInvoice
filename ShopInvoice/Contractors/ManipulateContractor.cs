﻿using ShopInvoiceClass;
using System;
using System.Windows.Forms;

namespace ShopInvoiceWidows.Contractors
{
    public partial class ManipulateContractor : Form
    {
        private int tmpID = -1;
        public ManipulateContractor()
        {
            InitializeComponent();
            formContractorID.Text = (DBContractor.LastId() + 1).ToString();
        }
        public ManipulateContractor(Contractor c)
        {
            InitializeComponent();
            tmpID = c.ID;
            formContractorID.Text = c.ID.ToString();
            formContractorName.Text = c.Name;
            formContractorAddress.Text = c.Address;
            formContractorNIP.Text = c.NIP;
            formContractorREGON.Text = c.REGON;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (tmpID < 0)
            {
                Contractor newContractor = new(DBContractor.LastId() + 1, formContractorName.Text, formContractorAddress.Text, formContractorNIP.Text, formContractorREGON.Text);
                DBContractor.Insert(newContractor);
            }
            else
            {
                Contractor newContractor = new(tmpID, formContractorName.Text, formContractorAddress.Text, formContractorNIP.Text, formContractorREGON.Text);
                DBContractor.EditByID(newContractor);
            }
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
