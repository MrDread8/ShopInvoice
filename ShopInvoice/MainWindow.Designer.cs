﻿
namespace ShopInvoice
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ContractorsButton = new System.Windows.Forms.ToolStripMenuItem();
            this.fakturyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InvoiceButton = new System.Windows.Forms.ToolStripMenuItem();
            this.nowaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.podgladToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductsButton = new System.Windows.Forms.ToolStripMenuItem();
            this.stanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stanToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inneOperacjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dostawaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pozaFakturoweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zestawienieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.formGrid = new System.Windows.Forms.DataGridView();
            this.formId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formContractor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formCharge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContractorsButton,
            this.InvoiceButton,
            this.ProductsButton,
            this.inneOperacjeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1198, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menu";
            // 
            // ContractorsButton
            // 
            this.ContractorsButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fakturyToolStripMenuItem,
            this.spisToolStripMenuItem});
            this.ContractorsButton.Name = "ContractorsButton";
            this.ContractorsButton.Size = new System.Drawing.Size(83, 20);
            this.ContractorsButton.Tag = "ContractorsButton";
            this.ContractorsButton.Text = "Kontrahenci";
            // 
            // fakturyToolStripMenuItem
            // 
            this.fakturyToolStripMenuItem.Name = "fakturyToolStripMenuItem";
            this.fakturyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fakturyToolStripMenuItem.Text = "Faktury";
            this.fakturyToolStripMenuItem.Click += new System.EventHandler(this.fakturyToolStripMenuItem_Click);
            // 
            // spisToolStripMenuItem
            // 
            this.spisToolStripMenuItem.Name = "spisToolStripMenuItem";
            this.spisToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.spisToolStripMenuItem.Text = "Spis";
            this.spisToolStripMenuItem.Click += new System.EventHandler(this.spisToolStripMenuItem_Click);
            // 
            // InvoiceButton
            // 
            this.InvoiceButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nowaToolStripMenuItem,
            this.usuńToolStripMenuItem,
            this.podgladToolStripMenuItem});
            this.InvoiceButton.Name = "InvoiceButton";
            this.InvoiceButton.Size = new System.Drawing.Size(58, 20);
            this.InvoiceButton.Tag = "InvoiceButton";
            this.InvoiceButton.Text = "Faktury";
            // 
            // nowaToolStripMenuItem
            // 
            this.nowaToolStripMenuItem.Name = "nowaToolStripMenuItem";
            this.nowaToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.nowaToolStripMenuItem.Text = "Nowa";
            this.nowaToolStripMenuItem.Click += new System.EventHandler(this.nowaToolStripMenuItem_Click);
            // 
            // usuńToolStripMenuItem
            // 
            this.usuńToolStripMenuItem.Name = "usuńToolStripMenuItem";
            this.usuńToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.usuńToolStripMenuItem.Text = "Usuń";
            this.usuńToolStripMenuItem.Click += new System.EventHandler(this.usuńToolStripMenuItem_Click);
            // 
            // podgladToolStripMenuItem
            // 
            this.podgladToolStripMenuItem.Name = "podgladToolStripMenuItem";
            this.podgladToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.podgladToolStripMenuItem.Text = "Podglad";
            this.podgladToolStripMenuItem.Click += new System.EventHandler(this.podgladToolStripMenuItem_Click);
            // 
            // ProductsButton
            // 
            this.ProductsButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stanToolStripMenuItem,
            this.stanToolStripMenuItem1});
            this.ProductsButton.Name = "ProductsButton";
            this.ProductsButton.Size = new System.Drawing.Size(67, 20);
            this.ProductsButton.Tag = "ProductsButton";
            this.ProductsButton.Text = "Produkty";
            // 
            // stanToolStripMenuItem
            // 
            this.stanToolStripMenuItem.Name = "stanToolStripMenuItem";
            this.stanToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.stanToolStripMenuItem.Text = "Strona";
            this.stanToolStripMenuItem.Click += new System.EventHandler(this.stanToolStripMenuItem_Click);
            // 
            // stanToolStripMenuItem1
            // 
            this.stanToolStripMenuItem1.Name = "stanToolStripMenuItem1";
            this.stanToolStripMenuItem1.Size = new System.Drawing.Size(108, 22);
            this.stanToolStripMenuItem1.Text = "Stan";
            this.stanToolStripMenuItem1.Click += new System.EventHandler(this.stanToolStripMenuItem1_Click);
            // 
            // inneOperacjeToolStripMenuItem
            // 
            this.inneOperacjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dostawaToolStripMenuItem,
            this.pozaFakturoweToolStripMenuItem,
            this.zestawienieToolStripMenuItem});
            this.inneOperacjeToolStripMenuItem.Name = "inneOperacjeToolStripMenuItem";
            this.inneOperacjeToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.inneOperacjeToolStripMenuItem.Text = "Magazyn";
            this.inneOperacjeToolStripMenuItem.Click += new System.EventHandler(this.inneOperacjeToolStripMenuItem_Click);
            // 
            // dostawaToolStripMenuItem
            // 
            this.dostawaToolStripMenuItem.Name = "dostawaToolStripMenuItem";
            this.dostawaToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.dostawaToolStripMenuItem.Text = "Dostawa";
            this.dostawaToolStripMenuItem.Click += new System.EventHandler(this.dostawaToolStripMenuItem_Click);
            // 
            // pozaFakturoweToolStripMenuItem
            // 
            this.pozaFakturoweToolStripMenuItem.Name = "pozaFakturoweToolStripMenuItem";
            this.pozaFakturoweToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.pozaFakturoweToolStripMenuItem.Text = "Poza fakturowe";
            this.pozaFakturoweToolStripMenuItem.Click += new System.EventHandler(this.pozaFakturoweToolStripMenuItem_Click);
            // 
            // zestawienieToolStripMenuItem
            // 
            this.zestawienieToolStripMenuItem.Name = "zestawienieToolStripMenuItem";
            this.zestawienieToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.zestawienieToolStripMenuItem.Text = "Zestawienie";
            this.zestawienieToolStripMenuItem.Click += new System.EventHandler(this.zestawienieToolStripMenuItem_Click);
            // 
            // formGrid
            // 
            this.formGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.formGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.formId,
            this.formName,
            this.formContractor,
            this.formCharge});
            this.formGrid.Location = new System.Drawing.Point(12, 27);
            this.formGrid.MultiSelect = false;
            this.formGrid.Name = "formGrid";
            this.formGrid.ReadOnly = true;
            this.formGrid.RowTemplate.Height = 25;
            this.formGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.formGrid.Size = new System.Drawing.Size(1174, 675);
            this.formGrid.TabIndex = 1;
            // 
            // formId
            // 
            this.formId.HeaderText = "ID";
            this.formId.Name = "formId";
            this.formId.ReadOnly = true;
            // 
            // formName
            // 
            this.formName.HeaderText = "Nazwa";
            this.formName.Name = "formName";
            this.formName.ReadOnly = true;
            this.formName.Width = 200;
            // 
            // formContractor
            // 
            this.formContractor.HeaderText = "Kontrachent";
            this.formContractor.Name = "formContractor";
            this.formContractor.ReadOnly = true;
            this.formContractor.Width = 200;
            // 
            // formCharge
            // 
            this.formCharge.HeaderText = "Suma";
            this.formCharge.Name = "formCharge";
            this.formCharge.ReadOnly = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1198, 737);
            this.Controls.Add(this.formGrid);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Faktury";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ContractorsButton;
        private System.Windows.Forms.ToolStripMenuItem InvoiceButton;
        private System.Windows.Forms.ToolStripMenuItem ProductsButton;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridView formGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn formId;
        private System.Windows.Forms.DataGridViewTextBoxColumn formName;
        private System.Windows.Forms.DataGridViewTextBoxColumn formContractor;
        private System.Windows.Forms.DataGridViewTextBoxColumn formCharge;
        private System.Windows.Forms.ToolStripMenuItem nowaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuńToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem podgladToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stanToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inneOperacjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dostawaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pozaFakturoweToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zestawienieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fakturyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spisToolStripMenuItem;
    }
}

