﻿
namespace ShopInvoiceWidows.InvoiceWindow
{
    partial class NewInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.kontrachentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produktToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodGrid = new System.Windows.Forms.DataGridView();
            this.formID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.formPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labl = new System.Windows.Forms.Label();
            this.lael = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ContractorName = new System.Windows.Forms.Label();
            this.ContractorRegon = new System.Windows.Forms.Label();
            this.ContractorNIP = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prodGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kontrachentToolStripMenuItem,
            this.produktToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(806, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // kontrachentToolStripMenuItem
            // 
            this.kontrachentToolStripMenuItem.Name = "kontrachentToolStripMenuItem";
            this.kontrachentToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.kontrachentToolStripMenuItem.Text = "Kontrahent";
            this.kontrachentToolStripMenuItem.Click += new System.EventHandler(this.kontrachentToolStripMenuItem_Click);
            // 
            // produktToolStripMenuItem
            // 
            this.produktToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.usunToolStripMenuItem});
            this.produktToolStripMenuItem.Name = "produktToolStripMenuItem";
            this.produktToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.produktToolStripMenuItem.Text = "Produkt";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.dodajToolStripMenuItem_Click);
            // 
            // usunToolStripMenuItem
            // 
            this.usunToolStripMenuItem.Name = "usunToolStripMenuItem";
            this.usunToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.usunToolStripMenuItem.Text = "Usun";
            this.usunToolStripMenuItem.Click += new System.EventHandler(this.usunToolStripMenuItem_Click);
            // 
            // prodGrid
            // 
            this.prodGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.prodGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.formID,
            this.formName,
            this.formCount,
            this.formPrice});
            this.prodGrid.Location = new System.Drawing.Point(12, 27);
            this.prodGrid.Name = "prodGrid";
            this.prodGrid.RowTemplate.Height = 25;
            this.prodGrid.Size = new System.Drawing.Size(582, 411);
            this.prodGrid.TabIndex = 1;
            this.prodGrid.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.prodGrid_CellEndEdit);
            // 
            // formID
            // 
            this.formID.HeaderText = "ID";
            this.formID.Name = "formID";
            // 
            // formName
            // 
            this.formName.HeaderText = "Nazwa";
            this.formName.Name = "formName";
            // 
            // formCount
            // 
            this.formCount.HeaderText = "Ilosc";
            this.formCount.Name = "formCount";
            // 
            // formPrice
            // 
            this.formPrice.HeaderText = "Cena";
            this.formPrice.Name = "formPrice";
            // 
            // labl
            // 
            this.labl.AutoSize = true;
            this.labl.Location = new System.Drawing.Point(600, 27);
            this.labl.Name = "labl";
            this.labl.Size = new System.Drawing.Size(42, 15);
            this.labl.TabIndex = 2;
            this.labl.Text = "Nazwa";
            // 
            // lael
            // 
            this.lael.AutoSize = true;
            this.lael.Location = new System.Drawing.Point(600, 55);
            this.lael.Name = "lael";
            this.lael.Size = new System.Drawing.Size(46, 15);
            this.lael.TabIndex = 3;
            this.lael.Text = "REGON";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(600, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "NIP";
            // 
            // ContractorName
            // 
            this.ContractorName.AutoSize = true;
            this.ContractorName.Location = new System.Drawing.Point(666, 27);
            this.ContractorName.Name = "ContractorName";
            this.ContractorName.Size = new System.Drawing.Size(38, 15);
            this.ContractorName.TabIndex = 5;
            this.ContractorName.Text = "label2";
            // 
            // ContractorRegon
            // 
            this.ContractorRegon.AutoSize = true;
            this.ContractorRegon.Location = new System.Drawing.Point(666, 55);
            this.ContractorRegon.Name = "ContractorRegon";
            this.ContractorRegon.Size = new System.Drawing.Size(38, 15);
            this.ContractorRegon.TabIndex = 6;
            this.ContractorRegon.Text = "label2";
            // 
            // ContractorNIP
            // 
            this.ContractorNIP.AutoSize = true;
            this.ContractorNIP.Location = new System.Drawing.Point(666, 81);
            this.ContractorNIP.Name = "ContractorNIP";
            this.ContractorNIP.Size = new System.Drawing.Size(38, 15);
            this.ContractorNIP.TabIndex = 7;
            this.ContractorNIP.Text = "label2";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(600, 308);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(200, 62);
            this.SaveButton.TabIndex = 8;
            this.SaveButton.Text = "Zapisz";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(600, 376);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(200, 62);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "Anuluj";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // NewInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 450);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.ContractorNIP);
            this.Controls.Add(this.ContractorRegon);
            this.Controls.Add(this.ContractorName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lael);
            this.Controls.Add(this.labl);
            this.Controls.Add(this.prodGrid);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "NewInvoice";
            this.Text = "Faktura";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prodGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem kontrachentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produktToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usunToolStripMenuItem;
        private System.Windows.Forms.DataGridView prodGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn formID;
        private System.Windows.Forms.DataGridViewTextBoxColumn formName;
        private System.Windows.Forms.DataGridViewTextBoxColumn formCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn formPrice;
        private System.Windows.Forms.Label labl;
        private System.Windows.Forms.Label lael;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ContractorName;
        private System.Windows.Forms.Label ContractorRegon;
        private System.Windows.Forms.Label ContractorNIP;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button Cancel;
    }
}