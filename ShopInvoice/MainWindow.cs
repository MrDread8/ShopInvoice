﻿using ShopInvoiceClass;
using ShopInvoiceWidows;
using ShopInvoiceWidows.InvoiceWindow;
using ShopInvoiceWidows.OtherOperations;
using ShopInvoiceWidows.Products;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace ShopInvoice
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            loadInvoices();
        }


        private void nowaToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            NewInvoice nw = new();
            nw.ShowDialog();
            loadInvoices();
        }
        private void loadInvoices()
        {
            formGrid.Rows.Clear();
            foreach (Invoice invo in DBInvoice.GetList())
            {
                formGrid.Rows.Add(invo.Id, invo.Name, DBContractor.GetByID(invo.ContractorID).Name, invo.Charge);
            }
        }

        private void usuńToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            DBInvoice.Remove((int)formGrid.SelectedRows[0].Cells[0].Value);
        }

        private void stanToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            ProductsWindow pw = new();
            this.Hide();
            pw.ShowDialog();
            this.Show();
            loadInvoices();
        }

        private void pozaFakturoweToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            OtherOperationWindow opw = new(2);
            this.Hide();
            opw.ShowDialog();
            this.Show();
        }

        private void dostawaToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            OtherOperationWindow opw = new(1);
            this.Hide();
            opw.ShowDialog();
            this.Show();
        }

        private void zestawienieToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Summary su = new();
            this.Hide();
            su.ShowDialog();
            this.Show();
        }

        private void podgladToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (formGrid.SelectedRows.Count < 1)
                return;
            String webaddr = "https://localhost:44387/Ivoice?id=" + formGrid.SelectedRows[0].Cells[0].Value;
            Process myProcess = new Process();

            try
            {
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = webaddr;
                myProcess.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void fakturyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ContractorWindow cw = new();
            cw.ShowDialog();
            formGrid.Rows.Clear();
            try
            {
                if (cw.GetID != 0)
                {
                    foreach (Invoice invo in DBInvoice.GetList(cw.GetID))
                    {
                        formGrid.Rows.Add(invo.Id, invo.Name, DBContractor.GetByID(invo.ContractorID).Name, invo.Charge);
                    }
                }
            }
            catch (Exception ex){
                MessageBox.Show(ex.Message, "Błąd",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }

        }

        private void spisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ContractorWindow cw = new();
            this.Hide();
            cw.ShowDialog();
            this.Show();
        }

        private void inneOperacjeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void zestawienieProduktuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductsWindow pw = new();
            pw.ShowDialog();
        }

        private void stanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String webaddr = "https://localhost:44387/index";
            Process myProcess = new Process();

            try
            {
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = webaddr;
                myProcess.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}